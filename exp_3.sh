#!/bin/bash
obsnum="$1"
taskset -ac 10 redis-server --port 7770 > /home/ec2-user/redis_1.log &
pidredis=$!
taskset -ac 11 redis-server --port 7771 > /home/ec2-user/redis_2.log &
taskset -ac 12 redis-server --port 7772 > /home/ec2-user/redis_3.log &
taskset -ac 13 redis-server --port 7773 > /home/ec2-user/redis_4.log &
taskset -ac 14 redis-server --port 7774 > /home/ec2-user/redis_5.log &
taskset -ac 15 redis-server --port 7775 > /home/ec2-user/redis_6.log &
taskset -ac 16 redis-server --port 7776 > /home/ec2-user/redis_7.log &
taskset -ac 17 redis-server --port 7777 > /home/ec2-user/redis_8.log &

sleep 10
cd STREAM
for i in {0..10}
do
echo "Run $i"
echo "Flushing redis"
redis-cli -p 7770 flushall
redis-cli -p 7771 flushall
redis-cli -p 7772 flushall
redis-cli -p 7773 flushall
redis-cli -p 7774 flushall
redis-cli -p 7775 flushall
redis-cli -p 7776 flushall
redis-cli -p 7777 flushall

echo "Resetting pqos"
timeout -s 2 2s sudo pqos -r > /dev/null
echo "Done resetting"
taskset -ac 3 sudo pqos -m llc:10-17 -u csv -o "/home/ec2-user/exp3_${obsnum}_obs${i}_redis.csv" &
pida=$!
taskset -ac 32 sudo pqos -m llc:29-31 -u csv -o "/home/ec2-user/exp3_${obsnum}_obs${i}_bench.csv" &
pidc=$!
echo $pida $pidc
pids=()
taskset -ac 24 redis-benchmark -p 7770 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_1.txt &
pids+=($!)
taskset -ac 25 redis-benchmark -p 7771 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_2.txt &
pids+=($!)
taskset -ac 26 redis-benchmark -p 7772 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_3.txt &
pids+=($!)
taskset -ac 27 redis-benchmark -p 7773 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_4.txt &
pids+=($!)
taskset -ac 28 redis-benchmark -p 7774 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_5.txt &
pids+=($!)
taskset -ac 29 redis-benchmark -p 7775 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_6.txt &
pids+=($!)
taskset -ac 30 redis-benchmark -p 7776 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_7.txt &
pids+=($!)
taskset -ac 31 redis-benchmark -p 7777 -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp3_${obsnum}_times_8.txt &
pids+=($!)
echo "Waiting for ${pids[@]}"
wait "${pids[@]}"

echo "Killing $pida $pidc"
sudo kill -2 "$pida"
sudo kill -2 "$pidc"
sudo killall -2 pqos
sleep 10
done
kill -2 "$pidredis"
killall redis-server
