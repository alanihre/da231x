#!/bin/bash
obsnum="$1"
taskset -ac 10 redis-server > /home/ec2-user/redis.log &
pidredis=$!
sleep 10
cd STREAM
for i in {0..10}
do
echo "Run $i"
echo "Flushing redis"
redis-cli flushall
echo "Resetting pqos"
timeout -s 2 2s sudo pqos -r > /dev/null
echo "Done resetting"
taskset -ac 11 ./stream_c.exe > /dev/null &
taskset -ac 12 ./stream_c.exe > /dev/null &
taskset -ac 13 ./stream_c.exe > /dev/null &
taskset -ac 14 ./stream_c.exe > /dev/null &
taskset -ac 15 ./stream_c.exe > /dev/null &
taskset -ac 16 ./stream_c.exe > /dev/null &
taskset -ac 17 ./stream_c.exe > /dev/null &
taskset -ac 18 ./stream_c.exe > /dev/null &
taskset -ac 19 ./stream_c.exe > /dev/null &
taskset -ac 20 ./stream_c.exe > /dev/null &
taskset -ac 21 ./stream_c.exe > /dev/null &
taskset -ac 22 ./stream_c.exe > /dev/null &
taskset -ac 23 ./stream_c.exe > /dev/null &
taskset -ac 2 sudo pqos -m llc:[11-23] -u csv -o "/home/ec2-user/exp2_${obsnum}_obs${i}_stream.csv" &
pidb=$!
taskset -ac 3 sudo pqos -m llc:10 -u csv -o "/home/ec2-user/exp2_${obsnum}_obs${i}_redis.csv" &
pida=$!
taskset -ac 25 sudo pqos -m llc:8 -u csv -o "/home/ec2-user/exp2_${obsnum}_obs${i}_bench.csv" &
pidc=$!
echo $pida $pidc $pidb
taskset -ac 24 redis-benchmark -d 400000 -r 10000 -n 100000 -t set,get -q --csv >> /home/ec2-user/exp2_${obsnum}_times.txt
echo "Killing $pida $pidc $pidb"
sudo kill -2 "$pida"
sudo kill -2 "$pidb"
sudo kill -2 "$pidc"
sudo killall -2 pqos
sudo killall stream_c.exe
sleep 10
done
kill -2 "$pidredis"
killall redis-server
