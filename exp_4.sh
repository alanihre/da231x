#!/bin/bash
obsnum="$1"
cd STREAM
for i in {0..0}
do
echo "Run $i"
echo "Resetting pqos"
timeout -s 2 2s sudo pqos -r > /dev/null
echo "Done resetting"
taskset -ac 11 ./stream_c.exe > /dev/null &
taskset -ac 12 ./stream_c.exe > /dev/null &
taskset -ac 13 ./stream_c.exe > /dev/null &
taskset -ac 14 ./stream_c.exe > /dev/null &
taskset -ac 15 ./stream_c.exe > /dev/null &
taskset -ac 16 ./stream_c.exe > /dev/null &
taskset -ac 17 ./stream_c.exe > /dev/null &
taskset -ac 18 ./stream_c.exe > /dev/null &
taskset -ac 5 sudo pqos -m llc:[11-18] -u csv -o "/home/ec2-user/exp2_${obsnum}_obs${i}_stream.csv" &
pidb=$!
taskset -ac 6 sudo pqos -m llc:0-3 -u csv -o "/home/ec2-user/exp2_${obsnum}_obs${i}_bfs.csv" &
pida=$!
echo $pida $pidb
#sudo -H -u ec2-user taskset -ac 10 perf record -o bfs_sing_perf.data -e LLC-loads,LLC-load-misses,LLC-stores,LLC-store-misses,cycles mpirun -n 8 /home/ec2-user/graph500/src/graph500_reference_bfs 20 | grep  "median_time" | egrep -oh  "[0-9]+\.[0-9]+" >> /home/ec2-user/exp2_${obsnum}_times.txt
taskset -ac 10 mpirun -n 8 /home/ec2-user/graph500/src/graph500_reference_bfs 20 | grep  "median_time" | egrep -oh  "[0-9]+\.[0-9]+" >> /home/ec2-user/exp2_${obsnum}_times.txt
echo "Killing $pida $pidb"
sudo kill -2 "$pida"
sudo kill -2 "$pidb"
sudo killall -2 pqos
sudo killall stream_c.exe
sleep 10
done
