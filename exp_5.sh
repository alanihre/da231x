#!/bin/bash
obsnum="$1"
for i in {0..0}
do
echo "Run $i"
echo "Resetting pqos"
timeout -s 2 2s sudo pqos -r > /dev/null
echo "Done resetting"
taskset -ac 8 sudo pqos -m llc:0-3 -u csv -o "/home/ec2-user/exp5_${obsnum}_obs${i}_bfs.csv" &
pida=$!
echo $pida
#sudo -H -u ec2-user taskset -ac 10 perf record -o bfs_mult_perf.data -e LLC-loads,LLC-load-misses,LLC-stores,LLC-store-misses,cycles mpirun -n 4 /home/ec2-user/graph500/src/graph500_reference_bfs 20 | grep  "median_time" | egrep -oh  "[0-9]+\.[0-9]+" >> /home/ec2-user/exp5_${obsnum}_times.txt
taskset -ac 10 mpirun -n 4 /home/ec2-user/graph500/src/graph500_reference_bfs 20 | grep  "median_time" | egrep -oh  "[0-9]+\.[0-9]+" >> /home/ec2-user/exp5_${obsnum}_times.txt

echo "Killing $pida"
sudo kill -2 "$pida"
sudo killall -2 pqos
done
