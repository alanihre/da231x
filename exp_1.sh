#!/bin/bash
cd STREAM
for i in {1..10}
do
echo "Run $i"
echo "Resetting pqos"
timeout -s 2 2s sudo pqos -r > /dev/null
echo "Done resetting"
taskset -ac 11 ./stream_c.exe > /dev/null &
taskset -ac 12 ./stream_c.exe > /dev/null &
taskset -ac 13 ./stream_c.exe > /dev/null &
taskset -ac 14 ./stream_c.exe > /dev/null &
taskset -ac 15 ./stream_c.exe > /dev/null &
taskset -ac 16 ./stream_c.exe > /dev/null &
taskset -ac 17 ./stream_c.exe > /dev/null &
taskset -ac 18 ./stream_c.exe > /dev/null &
taskset -ac 19 ./stream_c.exe > /dev/null &
taskset -ac 20 ./stream_c.exe > /dev/null &
taskset -ac 21 ./stream_c.exe > /dev/null &
taskset -ac 22 ./stream_c.exe > /dev/null &
taskset -ac 23 ./stream_c.exe > /dev/null &
taskset -ac 9 sudo pqos -m llc:[11-23] -u csv -o "/home/ec2-user/exp1_1_obs${i}_stream.csv" &
pidb=$!
taskset -ac 9 sudo pqos -m llc:10 -u csv -o "/home/ec2-user/exp1_1_obs${i}_bzip.csv" &
pida=$!
echo $pida $pidb
echo "Run $i" >> /home/ec2-user/exp1_1_times.txt
{ time taskset -ac 10 bzip2 -c9 < /dev/shm/test1 > /dev/null ; } 2>> /home/ec2-user/exp1_1_times.txt
echo "Killing $pida $pidb"
sudo kill -2 "$pida"
sudo kill -2 "$pidb"
sudo killall -2 pqos
sudo killall stream_c.exe
sleep 10
done
